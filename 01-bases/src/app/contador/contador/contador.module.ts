import { NgModule } from '@angular/core';
import { ContadorComponent } from '../../../contador.component';

@NgModule({
    declarations:[
       ContadorComponent
    ],
    exports:[
        ContadorComponent
    ],
    // Se exporta common cuando se usa ngFor ngIf
    // imports:[
    //     CommonModule
    // ]
})

export class ContadorModule {

}