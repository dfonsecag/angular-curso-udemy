import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchGifsResponse, Gif } from '../Interface/gifs.interfaces';

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  private apiKey:string = 'ox38QZsOhoTP8OI07Pz7aHU89tNNoARP&q';

  private servicioUrl = 'https://api.giphy.com/v1/gifs';

  private _historial:string[]= [];

  public resultados:Gif [] = [];
  
  get historial(){    
    return [...this._historial];
  }

  constructor(private http:HttpClient){
    
      this._historial =JSON.parse(localStorage.getItem('historial')!) || [];

      this.resultados =JSON.parse(localStorage.getItem('resultados')!) || [];
    
    
  }

  buscarGifs( query:string){

    query = query.trim().toLowerCase();
    // Para que no hayan datos repetidos en el arreglos
    if( !this._historial.includes(query) ){
      this._historial.unshift( query );
      this._historial = this._historial.splice(0,10);

      localStorage.setItem('historial', JSON.stringify(this._historial));

    }

    const params = new HttpParams()
      .set('api_key', 'ox38QZsOhoTP8OI07Pz7aHU89tNNoARP')
      .set('q', query)
      .set('limit', '10');

     this.http.get<SearchGifsResponse>(`${this.servicioUrl}/search`, { params:params })
    //  this.http.get<SearchGifsResponse>(`https://api.giphy.com/v1/gifs/search?api_ke=${this.apiKey}=${query}&limit=10`)
    .subscribe( (resp ) => {
      this.resultados = resp.data;
      localStorage.setItem('resultados', JSON.stringify(this.resultados));
    })
    
  }

}
